## Version
0.1.0

# Kenniscentrum Template
Template/sjabloom om automatisch gestandaardiseerde code repositories te maken. Werkt het beste voor Python, heeft ook functionaliteiten voor R maar ook voor andere programmeertalen kan dit nuttig zijn.

Auteurs: Jonathan Gerbscheid (hWh) en Sjoerd Gnodde (HH Delfland)

Uitgangspunten:
* Het is makkelijker om in andermans code wat te vinden als de structuur op elkaar lijkt.
* Als je aan het experimenteren bent en je weet niet of het ooit een package kan worden, is het handiger om alvast de structuur opgezet te hebben. Daarom adviseer ik deze template vaak te gebruiken.
* Als het even kan, lijken packages van verschillende programmeertalen op elkaar.

## Aan de slag met deze template
Cookiecutter draait op Python. Deze repository bevat één template repostory, waarmee je een lege repository kunt aanmaken met mappen, integraties etc.

Installeer de cookiecutter package via PyPI met het volgende in je prompt (bijvoorbeeld Anaconda Prompt) `pip install cookiecutter` of via Conda met `conda install -c conda-forge cookiecutter`. Dit hoeft alleen de eerste keer dat je hiermee aan de slag gaat.

Ga nu met je command prompt in de map staan waarin je het mapje met je repository maakt. Dit is waarschijnlijk de map waarin je andere repositories staan. Bij mij heet die bijvoorbeeld `.../git/`.
Zet nu de repository op via je command prompt: `cookiecutter https://gitlab.com/hetwaterschapshuis/kenniscentrum/kenniscentrum-template`

### Vragen
Na het invullen van het bovenstaande command, wordt je een aantal vragen gesteld. Vul steeds een antwoord in en sluit af met *Enter*. 

Tussen de blokhaken ***[ ]*** staat steeds het antwoord dat wordt gebruikt als je niks invult en meteen op *Enter* drukt.  

Sommige vragen zijn meerkeuze. Je dient dan het nummer van het bijbehorende antwoord in te vullen.

Hieronder staan alle vragen benoemd:

* `Full name`: Je naam en achternaam
* `reviewer_1`: Vul minstens één reviewer van je code in. Dit is de naam van een collega (naam en achternaam).
* `reviewer_2`: Eventueel ruimte voor een tweede reviewer.
* `email`: Jouw e-mailadres.
* `language`: Programmeer*taal*, bijvoorbeeld *python*.
* `cicd_pipelines`: Voor versiebeheer platform wil je CI/CD-pipelines aanmaken. Wij gebruiken eigenlijk altijd Azure.
* `repo_name`: Naam van de repository. Bij voorkeur alles lowercase, liggende streepjes (-) in plaats van spaties. Bijvoorbeeld *debiet-voorspeller-zuivering*.
* `project_short_description`: Korte omschrijving project.
* `pkg_name`: Vaak is de naam van de package een variant op de repository naam, maar het mag geen koppeltekens (-) bevatten. Advies is ook om geen hoofdletters te gebruiken. De package van *scikit-learn* heet bijvoorbeeld *sklearn*
* `url`: Als je URL van remote repository al weet, kun je die hier invullen. Het is altijd even goed nadenken hoe die eruit ziet, dus je kunt hem ook leeglaten en later invullen.
* `documentation`: Wel (*y*) of geen (*n*) documentatie klaarzetten. Voorkeur: ja!
* `version`: Welk versienummer wil je de eerste versie meegeven. Handigste is om dit met semantische versienummering te doen (X.Y.Z, zie: https://semver.org/), dan begin je met 0.1.0.
* `use_lfs`: Gebruik Git Life File Share (niet gebruiken als je hier niet mee bekend bent). Haalt .gitattributes weg en voegt een aantal bestandstypen toe aan de .gitignore. *y* voor ja en *n* voor nee. Zie: https://git-lfs.com/
* `python-requires`: Minimaal versienummer die andere van Python moeten hebben om jouw code te gebruiken.
* `license`: Voor interne projecten heb je geen licentie nodig.

### Afronding
Je zult het zelf nog onder versiebeheer met Git en op Azure DevOps moeten zetten.

## Tour door de template
Je kunt je nieuwe repository vinden in de map waar je in bent gaan staan om hem aan te maken onder de naam die je bij 'repo_name' hebt gegeven.

### <<pkg_name>>
Code die gemakkelijk aangeroepen kan worden op andere plekken, wordt een *package* genoemd. `sklearn` (scikit-learn) en `pandas` zijn bijvoorbeeld ook packages. Als je code schrijft die je vaker wil gaan gebruiken dan die ene toepassing (functies en classes), is het nuttig om het in de package te zetten. Dan kun je het installeren (voor Python `pip install -e .` in de map) en importeren in een ander stuk code (voor Python bijvoorbeeld `from <<pkg_name>> import <<module_name>> as <<gewenste_afkorting>>`). Het belangrijkste om te onthouden is dat ieder mapje een leeg `__init__.py` bestand nodig heeft.

Het advies is om dan ook meteen docstrings te gaan gebruiken, bijvoorbeeld in 
[numpy stijl](https://python.plainenglish.io/how-to-write-numpy-style-docstrings-a092121403ba).

Zie ook [Python packaging](https://packaging.python.org/en/latest/tutorials/packaging-projects/). Let op, hier gebruiken ze een dubbel getrapt `src/<<pkg_name>>`-structuur in plaats van alleen `<<pkg_name>>`.

### Versienummer
Bij iedere significante aanpassing (geen typfouten oplossen) verhoog je de versie. Het handigste is om dit met semantische versienummering te doen (X.Y.Z, zie: https://semver.org/), dan begin je met 0.1.0.

Het versienummer staat in [<<pkg_name>>/__version.py]({{cookiecutter.repo_name}}/{{cookiecutter.pkg_name}}/_version.py) (gebaseerd op deze antwoorden: [a](https://stackoverflow.com/a/7071358), [b](https://stackoverflow.com/a/459185)).



### CHANGELOG.md
Hou bij iedere verandering een versie, houd je een voeg je een stukje toe aan de changelog. Zo kunnen collega's snel zien wat er de afgelopen tijd gebeurd is. Alle `.md`-bestanden, zoals `CHANGELOG.md`, maken gebruik van de opmaak [Markdown](https://www.markdownguide.org/). 
Zie ook: [keepachangelog.com](https://keepachangelog.com/). 
Een stukje changelog kan er bijvoorbeeld zo uitzien:

```markdown
## [0.1.0] - 2023-01-02

### Added
* item 1
* item 2

### Removed
* item 3

```
De volgende kopjes worden normaal gebruikt:
* `[Added]` for new features.
* `[Changed]` for changes in existing functionality.
* `[Deprecated]` for soon-to-be removed features.
* `[Removed]` for now removed features.
* `[Fixed]` for any bug fixes.
* `[Security]` in case of vulnerabilities.


### .gitkeep
Wat doen al die *.gitkeep* bestanden in mijn repo? Nou, bij Git bestaan mapjes alleen als er een bestand in zit. Een *.gitkeep* bestandje is dus enkel een placeholder zodat je de mappenstructuur krijgt. Je zou deze dus kunnen verwijderen als je er een ander bestandje in zet.

### data
Hier kun je tijdelijk data neerzetten.

***LET OP!*** Het is niet de bedoeling om data te uploaden naar je Git remote. Zet alle data in de *.gitignore* (makkelijkst met `*.csv`, `*.txt`, `.xlsx` etc.). In uitzonderlijke gevallen kun je op je remote wel een kleine hoeveelheid data nodig hebben voor unit tests.

### docs
Hier kun je uitgebreidere documentatie schrijven, inclusief schematisaties etc.

### notebooks
Hier kun je in de experimenterende fase Python of R notebooks inzetten.

### output
Hier kun je alle output van je code naartoe sturen in de experimenterende fase. Bijvoorbeeld plots, voorspellingen, logs etc. Let op dat je ook hier geen data naar de remote pusht als je die daar niet nodig hebt; gebruik de *.gitignore*!

### pipelines
Hier komen een paar standaard CI/CD pipelines in te staan waarmee je code kan controleren op stijl en unit tests kan uitvoeren. Deze moet je in Azure eerst aanzetten voordat ze werken.
Let op: op dit moment (v0.1.0) zijn deze pipelines nog niet getest, enkel test-python.yaml (incl. code coverage) is offline getest.

### run
Als je code wilt maken om een model in productie wilt runnen, zonder dat het het model zelf is (want die staat bijvoorbeeld in de package met wat settings in een ander bestand), kun je dit in run zetten. Hierin staat de code meestal niet in functies.

### tests
Zet je unit tests hierin.

### .gitignore
Hierin zet je bestanden die je lokaal wel in je repository wilt, maar niet wilt uploaden. Zoals data (wachtwoorden kun je uit veiligheid beter helemaal niet in de map zetten).
[Leer meer over *.gitignore*](https://www.w3schools.com/git/git_ignore.asp).

### README.md
Hierin staat uitleg over de repository. Je leest nu bijvoorbeeld een README bestand. Het is geschreven in [Markdown](https://www.markdownguide.org/). Er is al een opzetje gemaakt door o.a. je naam en e-mailadres erin te zetten en uit te leggen hoe het installeren van de package werkt.

### conftest.py
Deze zorgt ervoor dat je [code coverage](https://en.wikipedia.org/wiki/Code_coverage) goed werkt.

### setup.py
Hiermee kun je de *package* installeren (alle andere code kun en hoef je niet te installeren). Deze wordt aangeroepen als je `pip install <<locatie_repo>>` uitvoert.

### setup.cfg
Hier staat wat informatie in die bijvoorbeeld de stijl controle gebruikt. Belangrijkste is dat je iets langere regels mag schrijven.

### Tips voor andere mappen
Je kunt zelf nog meer mappen toevoegen, bijvoorbeeld:
* **examples**: voorbeelden hoe de code gebruikt moet worden
* **img**: afbeeldingen voor in de README's.

## Support
For support please open an issue or send a mail to j.gerbscheid@hetwaterschapshuis.nl

## Contributing
For any suggestions or improvements please open an issue on the issue board or create a pull request.

## License
MIT

