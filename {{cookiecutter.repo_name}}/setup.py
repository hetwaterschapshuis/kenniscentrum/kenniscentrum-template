"""Setup the package."""
from setuptools import setup, find_packages

# Version number

import re
VERSIONFILE = "{{cookiecutter.pkg_name}}/_version.py"
verstrline = open(VERSIONFILE, "rt").read()
VSRE = r"^__version__ = ['\"]([^'\"]*)['\"]"
mo = re.search(VSRE, verstrline, re.M)
if mo:
    verstr = mo.group(1)
else:
    raise RuntimeError(f"Unable to find version string in {VERSIONFILE}.")


with open("README.md", "r") as fh:
    long_description = fh.read()

# Alle requirements
requirements = []

setup(
    name="{{cookiecutter.pkg_name}}",
    version=verstr,
    author="{{cookiecutter.full_name}}",
    author_email="{{cookiecutter.email}}",
    description="{{cookiecutter.project_short_description}}",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="{{cookiecutter.url}}",
    packages=find_packages(),
    install_requires=requirements,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: All rights reserved",
        "Operating System :: OS Independent",
        'Development Status :: 3 - Alpha'
    ],
    python_requires="{{cookiecutter.python_requires}}",
    # zip_safe=False
)
