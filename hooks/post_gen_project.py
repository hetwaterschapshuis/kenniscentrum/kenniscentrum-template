import os
import shutil
import datetime

print(f"Created the project in: {os.getcwd()}")

# Do conditional files/directories via: https://github.com/audreyr/cookiecutter/issues/723


def remove(filepath):
    if os.path.isfile(filepath):
        os.remove(filepath)
    elif os.path.isdir(filepath):
        shutil.rmtree(filepath)


def replace_in_file(filepath, to_replace, replacement):
    """used to replace dates/names in license files, not sure if
    there's a cookiecutter function to do this more cleanly.
    """
    # Read in the file
    with open(filepath, 'r') as file :
        filedata = file.read()

    # Replace the target string
    filedata = filedata.replace(to_replace, replacement)

    # Write the file out again
    with open(filepath, 'w') as file:
        file.write(filedata)


pkg_name = '{{cookiecutter.pkg_name}}'
use_lfs = '{{cookiecutter.use_lfs}}'
language = '{{cookiecutter.language}}'.lower()
cicd_pipelines = '{{cookiecutter.cicd_pipelines}}'.lower()
license = '{{cookiecutter.license}}'

current_year = str(datetime.datetime.now().year)

if (pkg_name == 'not applicable') | (language != 'python'):
    remove(os.path.join(os.getcwd(), '{{cookiecutter.pkg_name}}'))
    remove(os.path.join(os.getcwd(),  'setup.py'))
    remove(os.path.join(os.getcwd(),  'pipelines/build-python-package.yaml'))
else:
    print("- Please follow the instructions in 'pipelines/build-python-package.yaml' "
          "to set up an automatic package build")
    remove(os.path.join(os.getcwd(), 'src'))

if license == "MIT":
    remove(os.path.join(os.getcwd(),  'apache2.license'))
    remove(os.path.join(os.getcwd(),  'bsd3.license'))
    remove(os.path.join(os.getcwd(),  'GNU_3.0.license'))
    shutil.move(os.path.join(os.getcwd(),  'MIT.license'), 
                os.path.join(os.getcwd(),  'LICENSE'))
    replace_in_file(os.path.join(os.getcwd(),  'LICENSE'), '||current_date||', current_year)
elif license == "BSD-3":
    remove(os.path.join(os.getcwd(),  'apache2.license'))
    remove(os.path.join(os.getcwd(),  'GNU_3.0.license'))
    remove(os.path.join(os.getcwd(),  'MIT.license'))
    shutil.move(os.path.join(os.getcwd(),  'bsd3.license'), 
                os.path.join(os.getcwd(),  'LICENSE'))
    replace_in_file(os.path.join(os.getcwd(),  'LICENSE'), '||current_date||', current_year)
elif license == "GNU GPL v3.0":
    remove(os.path.join(os.getcwd(),  'apache2.license'))
    remove(os.path.join(os.getcwd(),  'bsd3.license'))
    remove(os.path.join(os.getcwd(),  'MIT.license'))
    shutil.move(os.path.join(os.getcwd(),  'GNU_3.0.license'), 
                os.path.join(os.getcwd(),  'LICENSE'))
    replace_in_file(os.path.join(os.getcwd(),  'LICENSE'), '||current_date||', current_year)
elif license == "Apache Software License 2.0":
    remove(os.path.join(os.getcwd(),  'bsd3.license'))
    remove(os.path.join(os.getcwd(),  'GNU_3.0.license'))
    remove(os.path.join(os.getcwd(),  'MIT.license'))
    shutil.move(os.path.join(os.getcwd(),  'apache2.license'), 
                os.path.join(os.getcwd(),  'LICENSE'))
    replace_in_file(os.path.join(os.getcwd(),  'LICENSE'), '||current_date||', current_year)
elif license == "no license/other license":
    remove(os.path.join(os.getcwd(),  'apache2.license'))
    remove(os.path.join(os.getcwd(),  'bsd3.license'))
    remove(os.path.join(os.getcwd(),  'GNU_3.0.license'))
    remove(os.path.join(os.getcwd(),  'MIT.license'))
    print("- Internal projects don't need a license, but consider adding one if you are going to share your code.")


if use_lfs != 'y':
    remove(os.path.join(os.getcwd(),  '.gitattributes'))
    with open(".gitignore", "a") as file_object:
        # Append 'hello' at the end of file
        file_object.write("\n# Data types\n")
        file_object.write("*.csv\n")
        file_object.write("*.xls\n")
        file_object.write("*.xlsx\n")
        file_object.write("*.xml\n")
        file_object.write("*.json\n")
        file_object.write("*.shp\n")
        file_object.write("*.cpg\n")
        file_object.write("*.dbf\n")
        file_object.write("*.prj\n")
        file_object.write("*.sbn\n")
        file_object.write("*.sbx\n")
        file_object.write("*.shp.xml\n")
        file_object.write("*.shx\n")
        file_object.write("\ndata/\n")

if cicd_pipelines == 'gitlab':
    remove(os.path.join(os.getcwd(), 'pipelines', '.build-python-package.yaml'))
    remove(os.path.join(os.getcwd(), 'pipelines', '.lint-python.yaml'))
    remove(os.path.join(os.getcwd(), 'pipelines', '.test-python.yaml'))
    remove(os.path.join(os.getcwd(), 'pipelines'))
elif cicd_pipelines == 'devops':
    remove(os.path.join(os.getcwd(), '.gitlab-ci.yaml'))
else:
    remove(os.path.join(os.getcwd(), 'pipelines', '.build-python-package.yaml'))
    remove(os.path.join(os.getcwd(), 'pipelines', '.lint-python.yaml'))
    remove(os.path.join(os.getcwd(), 'pipelines', '.test-python.yaml'))
    remove(os.path.join(os.getcwd(), 'pipelines'))
    remove(os.path.join(os.getcwd(), '.gitlab-ci.yaml'))

if language == 'r':
    remove(os.path.join(os.getcwd(),  'setup.cfg'))
    remove(os.path.join(os.getcwd(),  'conftest.py'))
    remove(os.path.join(os.getcwd(),  'pipelines/lint-python.yaml'))
    remove(os.path.join(os.getcwd(),  'pipelines/test-python.yaml'))

if language == 'python' and cicd_pipelines == 'devops':
    print("- Please create new 'Pipelines' in Azure Devops, and use the existing pipeline files "
          "found in the pipeline folder. A pipeline for each .yaml.")

if language == 'python' and cicd_pipelines == 'gitlab':
    print("- Please create new 'Pipelines' in gitlab, and use the existing pipeline file "
          "found in the root folder. Define the appropriate stages in the .yaml file.")

if ('{{cookiecutter.documentation}}' != 'y') | (language == 'r'):
    if language == "r":
        print('- The HWH cookiecutter part for automatic documentation in R is currently '
              'not supported.')
    print("Removing documentation files...")
    remove(os.path.join(os.getcwd(), 'docs', 'source'))
    remove(os.path.join(os.getcwd(), 'docs', 'build'))
else:
    print('- For information on how to create documentation: '
            "https://www.sphinx-doc.org/en/master/examples.html")
